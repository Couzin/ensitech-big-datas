const pm2 = require('pm2');
const fs = require('fs-extra');
require('dotenv').config();

const start = async () => {
  // getting amount of chunks by worker
  const totalChunks = fs.readdirSync("./chunks/csv").length;
  console.log(totalChunks)
  const filesPerWorker = Math.ceil(totalChunks / process.env.WORKERS_AMOUNT);
  const workers = [];

  for (let i = 0; i < process.env.WORKERS_AMOUNT; i++) {
    const fromFile = i * filesPerWorker;
    const toFile = Math.min((i + 1) * filesPerWorker - 1, totalChunks - 1);
    const worker = {
      name: `worker_${i}`,
      env: {
        "FROM_FILE": fromFile,
        "TO_FILE": toFile,
        "WORKER_NAME": `worker_${i}`
      }
    }
  // console.log("command", command);
    workers.push(worker);
  }


  for (let worker of workers) {
    pm2.start({
      script    : 'worker.js',
      name      : worker.name,
      env: worker.env,
      autorestart: false,
      node_args: ['--max-old-space-size=1024'],
      log_file: `./logs/${worker.name}.log`,
    }, function(err, apps) {
      if (err) {
        console.error(err)
        return pm2.disconnect()
      }
    })
  }
};

module.exports = start;