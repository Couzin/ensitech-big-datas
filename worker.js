const fs = require("fs");
const csv = require("convert-csv-to-json");
const parse = require('csv-parser');
const fromFile = parseInt(process.env.FROM_FILE)
const toFile = parseInt(process.env.TO_FILE)
const mongoose = require('mongoose');
const schema = require('./utils/model.js');

const DataModel = mongoose.model('company', schema);

// MongoDB connection setup
mongoose.connect('mongodb://localhost:26007/companies');

mongoose.connection.on('connected', () => {
  console.log('Connected to MongoDB');
});

mongoose.connection.on('error', (err) => {
  console.error('MongoDB connection error:', err);
});

// Function to parse CSV file and save data to MongoDB
const parseCSVAndSaveToDB = async (filePath) => {
  const results = [];
  const startTime = Date.now();
  try {
      const stream = fs.createReadStream(filePath)
          .pipe(parse())
          .on('data', (data) => {
            const cleanedData = {};
            Object.keys(data).forEach(key => {
                if (data[key] !== '') {
                    cleanedData[key] = data[key];
                }
            });
            results.push(cleanedData);
          })
          .on('end', async () => {
              // Save parsed data to MongoDB
              try {
                  await DataModel.insertMany(results);
                  console.log(`file ${filePath} registered`);
                  const minutes = Math.floor((Date.now() - startTime) / (1000 * 60));
                  const seconds = Math.floor(((Date.now() - startTime) % (1000 * 60)) / 1000);
                  const milliseconds = (Date.now() - startTime) % 1000;
                  console.log(`file ${filePath} processed in ${minutes}:${seconds},${milliseconds}`);
              } catch (err) {
                  console.error('Error saving data to MongoDB:', err);
              }
          })
          .on('error', (err) => {
              console.error('Error parsing CSV:', err);
          });
      
      await new Promise((resolve, reject) => {
          stream.on('end', resolve);
          stream.on('error', reject);
      });
  } catch (err) {
      console.error('Error processing file:', filePath, err);
  }
};

// Main function to process CSV files
const processCSVFiles = async (fromFile, toFile) => {
  const startTime = Date.now();
  for (let i = fromFile; i <= toFile; i++) {
      const filePath = `./chunks/csv/${i}.csv`;
      try {
          await parseCSVAndSaveToDB(filePath);
      } catch (error) {
          console.error('Error processing file:', filePath, error);
      }
  }
  const minutes = Math.floor((Date.now() - startTime) / (1000 * 60));
  const seconds = Math.floor(((Date.now() - startTime) % (1000 * 60)) / 1000);
  const milliseconds = (Date.now() - startTime) % 1000;
  console.log(`All files processed in ${minutes}:${seconds},${milliseconds}`);
};

// Start processing CSV files
processCSVFiles(fromFile, toFile).then(() => {
  console.log('All files processed successfully');
}).catch((err) => {
  console.error('Error processing files:', err);
});