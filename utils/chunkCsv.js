const splitStream = require("csv-split-stream");

const splitTime = Date.now();

return splitStream.split(
    fs.createReadStream("datas.csv"),
    { lineLimit: 10000 }, // todo (if I got time): try vary chunk size by cpu core amount
    (index) => fs.createWriteStream(`chunks/csv/${index}.csv`)
).then(csvSplitResponse => {
    const elapsedTime = Date.now() - splitTime;
    const minutes = Math.floor(elapsedTime / (1000 * 60));
    const seconds = Math.floor((elapsedTime % (1000 * 60)) / 1000);
    const milliseconds = elapsedTime % 1000;
    console.log("time", `${minutes}:${seconds},${milliseconds}`);
    console.log("csvSplitResponse", csvSplitResponse)
}).catch(csvSplitError => {
        console.error("csvSplitError", csvSplitError)
})