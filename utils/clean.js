const fs = require("fs-extra")

// Uncomment the following line to clean the entire chunks folder
// fs.rmSync("chunks", { recursive: true })
// fs.mkdirSync("chunks")

// Uncomment the following line to only clean the entire csv files
// fs.rmSync('chunks/csv', {recursive: true})
// fs.mkdirSync("chunks/csv")

//Uncomment the following line to only clean the entire logs files
fs.rmSync('logs', {recursive: true})
fs.mkdirSync("logs")