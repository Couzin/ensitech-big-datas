const defineShard = () =>  {
  return Math.ceil(Math.random() * 10);
}

const checkRepartition = () => {
  const shard = [];
  for (let i = 0; i < 1000000; i++) {
    shard.push(Math.random());
  }
  const average = shard.reduce((acc, curr) => acc + curr, 0) / shard.length;
  console.log(average);
}

checkRepartition();

