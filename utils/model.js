const mongoose = require('mongoose');
const generateShardKey = require('./generateShardKey');

const schema = new mongoose.Schema({
  shardKey: {
    type: String,
    required: true,
    default: generateShardKey(),
    index: true,
  },
  siren: String,
  nic: String,
  siret: String,
  dateCreationEtablissement: Date,
  dateDernierTraitementEtablissement: Date,
  typeVoieEtablissement: String,
  libelleVoieEtablissement: String,
  codePostalEtablissement: String,
  libelleCommuneEtablissement: String,
  codeCommuneEtablissement: String,
  dateDebut: Date,
  etatAdministratifEtablissement: String
});

module.exports = schema